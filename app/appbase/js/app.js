/**
 * Created by gaoxinkuan on 2017/2/14.
 */

var host = "http://het.aiplatform.com.cn/HETWeb/app/";

//跳转页面
function jumpAppPage(url, param) {

    var pageUrl;
    url.toLowerCase();
    if(url.search(/http/) != -1){
        pageUrl = url;
    }else {
        pageUrl = host + url;
    }

    var jumpData = {
        UA_ActionType: 0,
        UA_PopDepth: 0,
        UA_NewPage_URL: pageUrl,
        UA_Other: param
    };

    try {
        UA_Jump(JSON.stringify(jumpData));
    }catch (e){
        try {
            JavaFunc.UA_Jump(JSON.stringify(jumpData));
        }catch (e){
            location.href = pageUrl;

        }
    }
}

function jumpToAppNativePage(type) {
    var param = {
        "pageType":type
    };

    try {
        UA_JumpToNativePage(JSON.stringify(param));
    }catch (e){
        try {
            JavaFunc.UA_JumpToNativePage(JSON.stringify(param));
        }catch (e){
            // alert("方法未定义")
        }
    }
}





(function handleAhref() {
    window.onload = function () {
        var aList = document.getElementsByTagName('a');
        for (var i = 0;i < aList.length;i++){
            (function(){
                var a = aList[i];
                var href = a.getAttributeNode('href').value;
                a.getAttributeNode('href').value = 'javascript:void(0);';
                a.onclick=function () {
                    jumpAppPage(href,null);
                }
            })();
        }
    };
}());